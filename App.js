import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Constants from 'expo-constants';

// You can import from local files
import AssetExample from './components/AssetExample';
import Button from './components/Button';
import FormTextInput from './components/FormTextInput';

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>  
          <AssetExample />

          <FormTextInput info="Email"/>
          
          <FormTextInput info="Password"/>
        <Button/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   marginTop:40
   
  }
  
});
