import React, { Component } from 'react'
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native'

export default class Button extends Component {
  constructor(props) {
    super(props)
    
  }

  onPress = () => {
   
  }

 render() {
   return (
     <View style={styles.container}>
       <TouchableOpacity
         style={styles.button}
         onPress={this.onPress}>
         <Text style={styles.text}> Login </Text>
       </TouchableOpacity>
       
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 50,
    marginTop:60
  },
  button: {
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor:'rgb(71, 141, 226)',
    fontSize:40,
    fontWeight:'bold',
   borderRadius:10,
    height:50,
    padding: 10
  },
  text:{
    color:'white'
  }
 
})
